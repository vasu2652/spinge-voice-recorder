var express=require('express');
var app = express();
var port = 3000;
var path=require('path');
var webpack = require('webpack');
var config = require('./webpack.config');
var compiler = webpack(config);
var mkdirp=require('mkdirp');
var moment=require('moment')();
moment.format();
app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
}));
app.use('/uploads',express.static('uploads'));
app.get('/',function(req,res){
    res.sendFile(path.join(__dirname,'/src','index.html'));
});
app.post('/upload', (request, response, next) => {
  // parse a file upload
  var mime = require('mime');
  var formidable = require('formidable');
  var util = require('util');
  var form = new formidable.IncomingForm();
  var dir = !!process.platform.match(/^win/) ? '\\uploads\\' : '/uploads/';
  var path=`uploads/${moment.year()}/${moment.month()}/${moment.date()}/${moment.hour()}`;
    mkdirp(`${path}`, function (err) {
        if (err) console.error(err)
        else {
            form.uploadDir = __dirname +"/"+ path;
            form.keepExtensions = true;
            form.maxFieldsSize = 10 * 1024 * 1024;
            form.maxFields = 1000;
            form.multiples = false;
            form.parse(request, function(err, fields, files) {
                var file = util.inspect(files);
                response.writeHead(200, getHeaders('Content-Type', 'application/json'));
                var fileName = file.split('path:')[1].split('\',')[0].split(dir)[1].toString().replace(/\\/g, '').replace(/\//g, '');
                var fileURL = 'http://' +'localhost' + ':' + port + '/uploads/' + fileName;
                console.log('fileURL: ', fileURL);
                response.write(JSON.stringify({
                    fileURL: fileURL
                }));
                response.end();
  });
        }
    });
  
});
function getHeaders(opt, val) {
    try {
        var headers = {};
        headers["Access-Control-Allow-Origin"] = "https://secure.seedocnow.com";
        headers["Access-Control-Allow-Methods"] = "POST, GET, PUT, DELETE, OPTIONS";
        headers["Access-Control-Allow-Credentials"] = true;
        headers["Access-Control-Max-Age"] = '86400'; // 24 hours
        headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept";

        if (opt) {
            headers[opt] = val;
        }

        return headers;
    } catch (e) {
        return {};
    }
}
app.listen(port, ()=>{
  console.log("started on port"+port);
});
